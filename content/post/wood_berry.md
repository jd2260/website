---
title: "Interactive Wood Berry Distillation Simulation"
date: 2020-06-08T13:00:44-07:00
draft: false
math: true
description: "An interactive simulation of the Wood Berry Distillation model"
tags: [
    "chemical engineering",
    "process control",
    "simulation",
]
---

Hands on education is difficult to teach in chemical engineering. Pilot scale reactors and distillation columns are expensive and many universities are unable to afford them when starting a chemical engineering program and students getting time with this equipment in a laboratory can be difficult even when the university can afford it. With many universities going online in the Fall 2020 Semester due to COVID-19, engineering labs are forced to go online to allow students to interact with dynamic engineering systems. However, an online, interactive labratory for chemical engineering students is difficult for a number of reasons. 

# Distillation

Distillation is a fundamental part of many chemical process plants. Distillation is a complex system, with many variables. Each adjustable variable affects the output variable in varying proportions. Adjusting the reflux flowrate adjusts the composition of both the tops and the bottoms. Adjusting the feed rate to the column also affects the output composition and flowrate. 

## Building an Online, Interactive Lab

This is where things get difficult. The simulation needs to have the following requirements to be valuable for a chemical engineering labratory:

1. Interactive
2. Responsive
3. Allow the analysis of fuzzy data
4. Facilitate experimental design

A UI is frusterating to build and maintain, but many open source projects would make this quite easy. The data needs to be downloadable and saved for lab groups. For an interactive lab, the response time to user action needs to be almost instant so the user can instantly see the response after making an action. This makes solving a system of ODEs impossible due to speed issue and not knowing the users future actions. This simplifies the system but specifics can be calculated using simplified equations.

A derivation of the system of ordinary differential equations into a 2x2 system can be found [here](/assets/2016_ICCPEIC.pdf). The two input, two output system for distillation can be demonstrated by the following equation:
$$\begin{pmatrix}x_D\\\ x_B\end{pmatrix} =
G^{DV}(s)
\begin{pmatrix}R(s)\\\ S(s)\end{pmatrix}$$

Where $G^{DV}(s)$ represents a 2x2 transfer function matrix and: 

$$\begin{align}
&x_D - Distillate\ fraction \\\ &x_B - Bottoms\ fraction \\\ &R - Reflux\ flowrate \\\ &S - Steam\ flowrate
\end{align}$$

### Wood-Berry Model

Wood and Berry (1973) suggested the following equation for binary distillation of methanol and water in a 9 inch, 8-tray column. The trays are spaced 12 inches apart and are fitted with 2 1/4 x 1 7/8 in bubble caps arranged in a square pattern. 

$$\begin{pmatrix}x_D\\\ x_B\end{pmatrix} =
\begin{pmatrix}
\frac{12.8e^{-1s}}{16.7s+1} & \frac{-18.9e^{-3s}}{21.0s+1}
\\\ 
\frac{6.6e^{-7s}}{10.9s+1} & \frac{-19.4e^{-3s}}{14.4s+1}
\end{pmatrix}
\begin{pmatrix}R(s)\\\ S(s)\end{pmatrix} +
\begin{pmatrix}\frac{3.8e^{-8s}}{14.9s+1}\\\ \frac{4.9e^{-3.4s}}{13.2s+1}\end{pmatrix}*d
$$

Where $d$ represents the feed flowrate.

### Oguannaikke and Pay BDC Model

$$\begin{pmatrix}x_D\\\ x_B\end{pmatrix} =
\begin{pmatrix}
\frac{-21.6e^{-s}}{3.5s+1} & \frac{1.26e^{-0.3s}}{7.05s+1}
\\\ 
\frac{-2.75e^{-1.3s}}{8.2s+1} & \frac{-4.28e^{-0.3s}}{0.0s+1}
\end{pmatrix}
\begin{pmatrix}R(s)\\\ S(s)\end{pmatrix}$$

### Benzene - Toluene Separation BDC Model

$$\begin{pmatrix}x_D\\\ x_B\end{pmatrix} =
\begin{pmatrix}
\frac{0.04e^{-0.02s}}{s+0.237} & \frac{-0.022e^{-0.3s}}{s+0.178}
\\\ 
\frac{0.034e^{-0.3s}}{s+0.170} & \frac{-0.008e^{-0.1s}}{s+0.243}
\end{pmatrix}
\begin{pmatrix}R(s)\\\ S(s)\end{pmatrix} +
\begin{pmatrix}\frac{0.069e^{-0.3s}}{s+0.196}\\\ \frac{0.114e^{-0.3s}}{s+0.179}\end{pmatrix}*d
$$

## Future Work

Where does temperature modelling come into play? Can we calculate the composition at each tray? What happens if we want to increase the number of trays or the diameter of the column?

There are many improvements that can be made to this system.
 