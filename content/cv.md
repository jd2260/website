---
title: "CV"
type: "cv"
description: "Markdown Resume"
date: "2020-05-11"
aliases: ["cv","resume","contact"]
author: "Josh Donaldson"
menu: main
name: "About"
weight: 200
---

## Snapshot
-----------

I am a well-rounded chemical engineering graduate who is seeking to leverage my hands-on skills, education and passion for engineering to support an industry-leading employer. As a detail-oriented and high-performance team player, I offer core strengths in the areas of project management, data analysis, testing, and troubleshooting. What I most enjoy about engineering is the ability to solve complex problems and recommend process improvements that result in measurable outcomes.

A full PDF resume can be downloaded [here](/assets/resume.pdf)

**Specialties & Skills** | Chemical Engineering | Quality Assurance | Project Management   
Process Engineering & Design | Data Analysis & Reporting | Teamwork & Collaboration   
Contractor Relations | Client Relationship Management | Public Speaking & Presentations 

**Technical Proficincies** | Aspen Plus | SAP | PI Processbook | Microsoft Office Suite | Python | Golang   
Git | Matlab

## Professional Experience
--------------------

### Rio Tinto

#### Carbon Process Co-op

Led a team of operators to ensure high-quality production, project management, program delivery, and achievement of all key performance indicators (KPI). Oversaw processes to include induction furnace refractory measurements, cast iron calculations and equipment troubleshooting and support.

##### Key Achievements:

★ Coordinated activities within a critical cost savings program, resulting in $4.5MM in cost reductions over the next four years. Achieved a number of additional cost savings to include $300K per year by improving steel recycling processes.

★ Selected by leadership to serve as interim Carbon Process Technician. Served as the primary point of contact for process engineering support within the department.

★ Delivered leadership as a Project Manager with responsibility for reviewing existing equipment to identify ways in which to transform processes at minimal costs. Conducted risk assessments, implemented Kaizen and Black Belt strategies, performed cost analysis, and ultimately recognized by leadership for contributions.

### Craft Metrics

2018 - 2019, 2020 **Quality Control Engineer**

Within a start-up environment, reported directly to the Chief Technical Officer (CTO). Tasked with supporting a portfolio of 30+ clients within the academic and industrial service sectors.

##### Key Achievements:

★ Coordinated activities within the FermTools research project. Assessed various fermentation batches using Python and created machine learning algorithms to improve accuracy during batch processing.

★ Developed a data cleaning and batching algorithm to support client software usage for increased sales; created a second algorithm to model fermentation systems using reaction kinetics to improve approaches taken during industrial brewing.

★ Contributed additional expertise by working closely with clients to deliver technical support and assistance, creating system documentation to support future cohorts of co-op students, and by acquiring grant funding from the Canadian government to support program activities.

## Volunteerism
---------------
**Stage Manager (2012-2014) |** As one of two supervisors on the team, managed a cast and crew to achieve the successful completion of a show run. Held full responsibility for issues during the run, wrote nightly reports to support the decision-making process and presented findings to owners and directors to improve processes going forward.

## Education
---------

2015-2020 **BASc, Chemical (Process) Engineering**; UBC (Vancouver)

2014-2015 **BSc, General**; UBCO (Kelowna)

**Relevant Courses |** Process Control, Chemical Engineering Laboratory, Chemical Engineering Thermodynamics, Fluid Mechanics, Heat Transfer, Interfacial Phenomena, Electrochemistry, Process Design, Process Synthesis, TensorFlow, DevOps

## Professional Affliations
---------

- **ENGINEERS AND GEOSCIENTISTS OF BRITISH COLUMBIA** (EGBC) (Engineer-in-Training)
- **AMERICAN INSTITUTE OF CHEMICAL ENGINEERING** (AICHE) (Member) 
- **AMERICAN SOCIETY OF BREWING CHEMISTS** (Member)
