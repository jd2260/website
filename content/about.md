---
title: "About"
description: "About Joshua Donaldson"
date: "2020-05-17"
aliases: ["about"]
author: "Josh Donaldson"
menu: main
name: "About"
weight: 300
---

# About Me

I am a Chemical Engineer from UBC. Born and raised in Kelowna, BC, my hobbies include snowboarding, playing bass guitar, cooking, working on various programming projects, and drinking (or making) beer. 

I am currently seeking fulltime employment while working on some side projects such as contracting work for [Craft Metrics](https://www.craftmetrics.ca), and assisting with projects with UBC Research Group [The Biofoundry](http://biofoundry.sites.olt.ubc.ca/). I am teaching myself some general DevOps skill such as Docker, working on a Natural Language Processing project (more coming soon), and designing a Virtual Simulation Lab.

Contact me about web development, projects that contain process engineering, data analysis or a mixture of both! 
